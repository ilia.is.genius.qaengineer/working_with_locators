package com.skillup;

public class LocatorsClass {

    /* LinkedIn Page: https://www.linkedin.com/start/join */

    //First Name Input
    public static final String FIRST_NAME_INPUT_XPATH_LOCATOR = "//*[@name = 'firstName']";
    public static final String FIRST_NAME_INPUT_CSS_LOCATOR = "#first-name";

    //Last Name Input
    public static final String LAST_NAME_INPUT_XPATH_LOCATOR = "//*[@name = 'lastName']";
    public static final String LAST_NAME_INPUT_CSS_LOCATOR = "#last-name";

    //Email Input
    public static final String EMAIL_INPUT_XPATH_LOCATOR = "//*[@name = 'emailAddress']";
    public static final String EMAIL_INPUT_CSS_LOCATOR = "#join-email";

    //Password Input
    public static final String PASSWORD_INPUT_XPATH_LOCATOR = "//*[@type = 'password']";
    public static final String PASSWORD_INPUT_CSS_LOCATOR = "#join-password";

    //User Agreement Link
    public static final String USER_AGREEMENT_LINK_XPATH_LOCATOR = "//a[contains(@href, 'user-agreement')]";
    public static final String USER_AGREEMENT_LINK_INPUT_CSS_LOCATOR = "a[href *= 'user-agreement']";

    //Privacy Policy Link
    public static final String PRIVATE_POLICY_LINK_XPATH_LOCATOR = "//a[contains(@href, 'privacy-policy')]";
    public static final String PRIVATE_POLICY_LINK_CSS_LOCATOR = "a[href *= 'privacy-policy']";

    //Cookie Policy Link
    public static final String COOKIE_POLICY_LINK_XPATH_LOCATOR = "//a[contains(href, 'cookie-policy')]";
    public static final String COOKIE_POLICY_LINK_CSS_LOCATOR = "a[href *= 'cookie-policy']";

    //Agree&join Button
    public static final String AGREE_JOIN_BUTTON_XPATH_LOCATOR = "//*[contains(@class, 'join-btn')]";
    public static final String AGREE_JOIN_BUTTON_CSS_LOCATOR = ".join-btn";

    //Sigh In Link
    public static final String SIGH_IN_LINK_XPATH_LOCATOR = "//a[contains(@href, 'join-sign-in')]";
    public static final String SIGN_IN_LINK_CSS_LOCATOR = "a[href *= 'join-sign-in']";



    /* LinkedIn Page: https://www.linkedin.com/uas/login */

    //Email or Phone Input
    public static final String EMAIL_OR_PHONE_INPUT_XPATH_LOCATOR = "//*[@name = 'session_key']";
    public static final String EMAIL_OR_PHONE_INPUT_CSS_LOCATOR = "#username";

    //Password Input
    public static final String PASSWORD_INPUT2_XPATH_LOCATOR = "//*[@name = 'session_password']";
    public static final String PASSWORD_INPUT2_CSS_LOCATOR = "#password";

    //Show span visibility
    public static final String SHOW_SPAN_VISIBILITY_XPATH_LOCATOR = "//*[@class = 'button__password-visibility']";
    public static final String SHOW_SPAN_VISIBILITY_CSS_LOCATOR = ".button__password-visibility";

    //Sigh In Button
    public static final String SIGH_IN_BUTTON_XPATH_LOCATOR = "//*[@class = 'login__form']//button";
    public static final String SIGH_IN_BUTTON_CSS_LOCATOR = ".login__form button";

    //Forgot Password Link
    public static final String FORGOT_PASSWORD_LINK_XPATH_LOCATOR = "//a[contains(@href, 'request-password-reset')]";
    public static final String FORGOT_PASSWORD_LINK_CSS_LOCATOR = "a[href *= 'request-password-reset']";

    //Join Now Link
    public static final String JOIN_NOW_LINK_XPATH_LOCATOR = "//*[@href = '/start/join']";
    public static final String JOIN_NOW_LINK_CSS_LOCATOR = "[href = '/start/join']";

    //User Agreement Link
    public static final String USER_AGREEMENT_LINK2_XPATH_LOCATOR = "//a[contains(@href, 'user-agreement')]";
    public static final String USER_AGREEMENT_LINK2_CSS_LOCATOR = "a[href *= 'user-agreement']";

    //Private Policy Link
    public static final String PRIVATE_POLICY_LINK2_XPATH_LOCATOR = "//a[contains(@href, 'privacy-policy')]";
    public static final String PRIVATE_POLICY_LINK2_CSS_LOCATOR = "a[href *= 'privacy-policy']";

    //Community Guidelines Link
    public static final String COMMUNITY_GUIDELINES_LINK_XPATH_LOCATOR = "//a[contains(@href, 'community_guidelines')]";
    public static final String COMMUNITY_GUIDELINES_LINK_CSS_LOCATOR = "a[href *= 'community_guidelines']";

    //Cookie Policy Link
    public static final String COOKIE_POLICY_LINK2_XPATH_LOCATOR = "//a[contains(@href, 'cookie-policy')]";
    public static final String COOKIE_POLICY_LINK2_CSS_LOCATOR = "a[href *= 'cookie-policy']";

    //Copyright Policy Link
    public static final String COPYRIGHT_POLICY_LINK_XPATH_LOCATOR = "//a[contains(@href, 'copyright-policy')]";
    public static final String COPYRIGHT_POLICY_LINK_CSS_LOCATOR = "a[href *= 'copyright-policy']";

    //Send Feedback Link
    public static final String SEND_FEEDBACK_LINK_XPATH_LOCATOR = "//a[contains(@href, 'send_feedback')]";
    public static final String SEND_FEEDBACK_LINK_CSS_LOCATOR = "a[href *= 'send_feedback']";
}
