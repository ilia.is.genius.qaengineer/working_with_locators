package com.skillup;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class MyTest extends TestRunner {

    private static final String URL = "https://www.linkedin.com/start/join";

    @Test
    public void myTest3() {
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(100000, TimeUnit.MILLISECONDS);

        driver.findElement(By.xpath(LocatorsClass.FIRST_NAME_INPUT_XPATH_LOCATOR)).sendKeys("My group is best");
        driver.findElement(By.cssSelector(LocatorsClass.AGREE_JOIN_CSS_LOCATOR)).click();
    }
}
